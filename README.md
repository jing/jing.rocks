# About

## Self introduction taken from /about

My name is Jing Luo. I'm fluent in Chinese (Mandarin), English, and
Japanese. In my free time, I work on free software projects and promote free
software, free culture, and the democratization of technology.

See also: [What is Free Software?](https://www.gnu.org/philosophy/free-sw.html)

My roles in the GNU project: gnu.org Chinese & Japanese translation team
member (2023-08~). GNU webmaster (2023-10~).

I grant myself the rank of webmaster, postmaster, and hostmaster.

## My Personal Website

This's my tech blog, a website built from Jekyll. Do you need more
explanation? Really?

## Catch-all license

Copyright 2023, 2024, Jing Luo.

### Code and their documents, unless specified

GNU GPL v3.0 or later

ABSOLUTELY NO WARRANTY, not even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.

[![License: GPL v3](/assets/license-badges/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

### Non-software/non-code (literature, etc.), unless specified

[![CC BY-SA 4.0][cc-by-sa-shield]][cc-by-sa]

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0
International License][cc-by-sa].

ABSOLUTELY NO WARRANTY, not even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.

[![CC BY-SA 4.0][cc-by-sa-image]][cc-by-sa]

[cc-by-sa]: http://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-shield]: /assets/license-badges/License-CC-BY-SA-4.0-lightgrey.svg

### Get the source code

The source code of this website is hosted
[here](https://forgejo.jing.rocks/jing/jing.rocks), using the free software
forge called [forgejo](https://forgejo.org/); you don't need an account to
check out the source code. If you cannot or do not want to run Javascript in
your browser, you can browse the source code using
[Cgit](https://git.jing.rocks/cgit/jing/jing.rocks.git/) or
[Gitweb](https://git.jing.rocks/gitweb/?p=jing/jing.rocks.git;a=summary).
